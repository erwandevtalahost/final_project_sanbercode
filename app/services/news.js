import { client, client2 } from "./client";



export const newsGetAll = async () => client2.get(`/products`);

export const newsAdd = async (title, value) =>
	client.post(`/news`, { title, value });

export const newsEdit = async (id, title, value) =>
	client.put(`/news/${id}`, { title, value });

export const newsDelete = async (id) => client.delete(`/news/${id}`);
