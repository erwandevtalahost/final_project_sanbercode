import { client } from "./client";


export const loginUser = ({ email, password }) =>
	client.post("/login", { email, password });

// Validasi token dengan cara request
// profile user dari token yang dimiliki
export const me = async ({ token }) =>
	client.get("/api/users/2", {
		headers: {
			Authorization: `Bearer ${token}`,
		},
	});

export const registerUser = ({ email, name, password }) =>
	client.post("/users", { email, name, password });
