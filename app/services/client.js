import axios from "axios";
import { store } from "../store";
import * as SecureStore from "expo-secure-store";

export const baseURL = "https://reqres.in/api";
export const baseURL2 = "https://fakestoreapi.com";

export const client = axios.create({
	baseURL: baseURL
});

export const client2 = axios.create({
	baseURL: baseURL2,
});

// Intercept akan dijalankan setiap kali ada request
// Otomatis mengambil data token dari storage dan menempelkannya
// ke header, sehingga saat request kita tidak perlu menempelkan
// token berulang kali
// SecureStore.setItemAsync("token", "QpwL5tke4Pnpja7X4");

client.interceptors.request.use(async (config) => {
	const token = store.getState().auth.token;
	

	if (token) {
		// eslint-disable-next-line no-param-reassign
		config.headers.Authorization = `Bearer ${token}`;
		// config.headers.Authorization = `Bearer QpwL5tke4Pnpja7X4`;
	}else{
		// config.headers.Authorization = `Bearer QpwL5tke4Pnpja7X4`;
		console.log("Tidak Ditemukan Token Woi")
	}

	return config;
});
