import * as React from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import NewsListScreen from '../features/news/NewsListScreen';
import Profil from './Profil';


const Tab = createBottomTabNavigator();

export default function BottomTab() {
  return (
   
      <Tab.Navigator>
        <Tab.Screen name="Home" component={NewsListScreen} />
        <Tab.Screen name="Profil" component={Profil} />
      </Tab.Navigator>
  
  );
}