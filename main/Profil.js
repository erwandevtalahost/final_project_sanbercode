import React,{useState} from "react";
import { StyleSheet,Text,View,Image,SafeAreaView,ScrollView,Button } from "react-native";
import { logout } from "../features/auth/authSlice"
import { useDispatch } from "react-redux";






export default function Profil() {
    const dispatch = useDispatch();

    const onLogout = async () => {
		try {
			

			dispatch(
				// Dispatch token dari response api
				logout({
					token: "",
					
				}),
			);
		} catch (error) {
			console.error(error)
			// Alert.alert("Login Gagal", error.response.data.message);
		}
	};
  return (
    <SafeAreaView style={styles.container}>
        <ScrollView>
            <View style={styles.container}>
                <Text style={styles.title}>Tentang Saya</Text>
                <View style={styles.subtitle}>
                    <Image
                        style={styles.imageLogo}
                        source={{
                            uri : "https://img.icons8.com/color/48/000000/bad-bunny.png"
                        }}
                    />

                </View>
                <>
                    <Text style={styles.name}>Erwansyah</Text>
                    <Text style={styles.jobs}>React Native Developer</Text>
                </>
                <View style={styles.portofolio}>
                    <View style={styles.contentPortofolio}>
                        <Text>Portofolio</Text>
                        <View style={styles.contentSkill}>
                            <View style={styles.subContentSkill}>
                            <Image source={require("../assets/icons8-gitlab-48.png")}/>
                                <Text>erwandevtalahost</Text>

                            </View>
                            <View style={styles.subContentSkill}>
                            <Image source={require("../assets/icons8-github-48.png")}/>
                                <Text>hun7ok</Text>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={styles.socialMedia}>
                    <View style={styles.contentMediaSosial}>
                        <Text>Hubungi Saya</Text>
                        <View style={styles.contentMediaSosialDown}>
                            <View style={styles.subContentMediaSosialDown}>
                                <>
                                <Image source={require("../assets/icons8-facebook-48.png")}/>
                                    <Text>facebook </Text>
                                </>
                                <>
                                <Image source={require("../assets/icons8-twitter-48.png")}/>
                                    <Text>Twitter</Text>
                                </>
                                <>
                                    <Image source={require("../assets/icons8-instagram-64.png")}/>
                                    <Text>Instagram</Text>
                                </>

                            </View>

                        </View>
                    </View>


                </View>
                

            </View>
            <Button title="SignOut" onPress={onLogout} />
        </ScrollView>

    </SafeAreaView>
  )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: "white"
    },
    scrollView: {
        backgroundColor: 'pink',
        marginHorizontal: 20
    },
    imageLogo: {
        height: 136,
        width: 136,
        resizeMode: "stretch",
        marginBottom: 20,
        alignSelf: "center",
        marginTop: 20
    },
    title: {
        fontSize: 36,
        fontWeight: "700",
        marginBottom: 20,
    },
    subtitle: {
        backgroundColor: "grey",
        height: 200,
        width: 200,
        borderRadius: 100,
    },
    name: {
        fontSize: 24,
        fontWeight: "700",
        color: "blue"
    },
    jobs: {
        fontSize: 16,
        fontWeight: "400",
        color: "blue",
    },
    portofolio: {
        backgroundColor: "#EFEFEF",
        height: 140,
        width: 300
    },
    contentPortofolio: {
        padding: 5
    },
    contentSkill: {
        padding: 10,
        flexDirection: "row",
        justifyContent: "space-around",
        height: 90,
        marginTop: 10
    },
    subContentSkill: {
        flexDirection: "column",
        paddingBottom: 5
    },
    socialMedia: {
        marginTop: 10,
        backgroundColor: "#EFEFEF",
        height: 300,
        width: 300,
    },
    contentMediaSosial: {
        padding: 8
    },
    contentMediaSosialDown: {
        flexDirection: "column",
        justifyContent: "space-around",
    },
    subContentMediaSosialDown: {
        alignSelf: "center",
        marginTop: 10
    }
})