import { NavigationContainer } from "@react-navigation/native";
import React from "react";
import { Provider } from "react-redux";
// import { store } from "./app/store";

import { store } from "../app/store"




// import { RootStack } from "./navigator/RootStack";


import { RootStack } from "../navigator/RootStack"

export default function Init()  {
	return (
		<Provider store={store}>
			
				<RootStack />
			
		</Provider>
	);
};


