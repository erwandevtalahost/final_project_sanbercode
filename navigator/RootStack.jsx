import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { useSelector } from "react-redux";

import LoginScreen from "../features/auth/LoginScreen";
import RegisterScreen from "../features/auth/RegisterScreen";
import SplashScreen from "../features/auth/SplashScreen";
import NewsListScreen from "../features/news/NewsListScreen";
import DetailBarang from "../features/news/DetailBarang"

import BottomTab from "../main/BottomTab";

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

export const  RootStack = () =>  {
	const isSignedIn = useSelector((state) => state.auth.isSignedIn);


	return (
		<NavigationContainer>
		
		<Stack.Navigator>
			{isSignedIn ? ( 
				<>
				<Stack.Screen
						name="BottomTab"
						component={BottomTab}
						options={{ headerShown: false }}
        		/>
				{/* <Stack.Screen name="MenuBottonDown" component={bottomTab}/> */}
				{/* <Stack.Screen name="NewsListScreen" component={NewsListScreen} /> */}
				<Stack.Screen name="DetailBarang" component={DetailBarang}/>
			
				</>
				
				
			) : (
				<>
					<Stack.Screen
						name="SplashScreen"
						component={SplashScreen}
						options={{ headerShown: true }}
					/>
					<Stack.Screen name="LoginScreen" component={LoginScreen} />
					
				</>
			)}
		</Stack.Navigator>
		</NavigationContainer>
	);
};
