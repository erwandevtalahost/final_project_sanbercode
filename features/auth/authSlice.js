import { createSlice } from "@reduxjs/toolkit";
import * as SecureStore from "expo-secure-store";

const initialState = {
	isSignedIn: false,
	token: "",
};

// async function save(key, value) {
// 	await SecureStore.setItemAsync(key, value);
//   }

export const authSlice = createSlice({
	name: "auth",
	initialState,
	reducers: {
		 login:  (state, action) => {
			const { token } = action.payload;

			state.isSignedIn = true;
			state.token = token;

			// save("token",token)
			SecureStore.setItemAsync("token", token);
			// SecureStore.setItemAsync(key, value);
			
		    
			
			
		},
		logout: (state) => {
			state.isSignedIn = false;
			state.token = "";

			SecureStore.deleteItemAsync("token");
			// SecureStore.deleteItemAsync("tok")
		},
	},
});

export const { login, logout } = authSlice.actions;
