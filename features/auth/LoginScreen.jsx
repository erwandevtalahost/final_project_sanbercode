import { useState } from "react";
import { Alert, Button, StyleSheet, Text, TextInput, View,Image } from "react-native";
import { useDispatch } from "react-redux";

import { loginUser } from "../../app/services/auth";
import { login } from "./authSlice";



const LoginScreen = ({ navigation }) => {
	const dispatch = useDispatch();

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const onLogin = async () => {
		try {
			const res = await loginUser({
				email,
				password,
			});

			dispatch(
				// Dispatch token dari response api
				login({
					token: res.data?.token,
					
				}),
			);
		} catch (error) {
			console.error(error)
			Alert.alert("Login Gagal", error.response.data.message);
		}
	};

	return (
		<View style={styles.container}>
			<View >
			<Image
                        style={styles.imageLogo}
                        source={require("../../assets/e_commerce.jpeg")}
                    />

                </View>
			<Text style={styles.title}>Login</Text>
			<TextInput
				style={styles.textInput}
				value={email}
				onChangeText={setEmail}
				placeholder="Masukkan email anda"
			/>
			<TextInput
				style={styles.textInput}
				value={password}
				onChangeText={setPassword}
				placeholder="Masukkan password anda"
			/>
			<Button title="Login" onPress={onLogin} />

			<View >
                    <Image
                        style={styles.imageLogo}
                        source={require("../../assets/logo_sanber.jpeg")}
                    />

                </View>
			
		</View>
	);
};

export default LoginScreen;

const styles = StyleSheet.create({
	container: {
		backgroundColor: "white",
		flex: 1,
		justifyContent: "center",
		padding: 20,
	},
	imageLogo: {
        height: 136,
        width: 136,
        resizeMode: "stretch",
        marginBottom: 20,
        alignSelf: "center",
        marginTop: 20,
		borderRadius: 400/ 2
    },
	title: {
		alignSelf: "center",
		fontSize: 24,
		fontWeight: "bold",
		marginBottom: 10,
	},
	textInput: {
		borderRadius: 10,
		borderWidth: 1,
		paddingHorizontal: 15,
		paddingVertical: 7,
		marginBottom: 10,
	},
});
