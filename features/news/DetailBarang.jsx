import { Text, View, Button,SafeAreaView,Image,StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';


export default function DetailBarang({ route, navigation }) {
    /* 2. Get the param */
    const { itemId } = route.params;
    const { otherParam } = route.params;
    const { gambar } = route.params;
    const { harga } = route.params;
    console.log("ini gambar y lh cuy",gambar)

    
  
    return (
      <SafeAreaView>
        <View>
          <Image
            style={styles.image}
            source={{
              uri : gambar
            }}
           
          />
          <Text style={[styles.text,styles.blueText]}>{otherParam}</Text>
          <Text style={[styles.text,styles.blueText]}>{harga}</Text>

        </View>

      </SafeAreaView>
    );
  }



  const styles = StyleSheet.create({
    text: {
      fontSize: 24,
      textAlign: "center",
      marginHorizontal: 16,
      marginTop: 12,
    },
    blueText: {
      color: "blue",
    },
    greyText: {
      color: "grey",
    },
    image: {
      width: 300,
      alignSelf: "center",
      height: 300,
     borderWidth: 1,
    
     resizeMode: "contain"
    

    },
  });