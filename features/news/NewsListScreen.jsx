import { useFocusEffect } from "@react-navigation/native";
import { useCallback, useEffect, useState } from "react";
import { logout } from "../auth/authSlice"
import { useDispatch } from "react-redux";

import {
	Alert,
	Button,
	FlatList,
	StyleSheet,
	Text,
	TextInput,
	View,
} from "react-native";

import { newsAdd, newsEdit, newsGetAll } from "../../app/services/news";
import NewsItem from "./NewsItem";

export default function NewsListScreen({ navigation }) {
	const [isLoading, setIsLoading] = useState(false);
	const dispatch = useDispatch();

	const [title, setTitle] = useState("");
	const [value, setValue] = useState("");
	const [image, setImage] = useState("");

	const [news, setNews] = useState([]);
	const [selectedNews, setSelectedNews] = useState({});

	const handleError = (err) => {
		console.warn("Error Status: ", err.message);
		console.warn("Error Message: ", err.response.data);
		// Alert.alert(`Error ${err.message}`, err.response.data.message);
	};

	const onLogout = async () => {
		try {
			

			dispatch(
				// Dispatch token dari response api
				logout({
					token: "",
					
				}),
			);
		} catch (error) {
			console.error(error)
			// Alert.alert("Login Gagal", error.response.data.message);
		}
	};

	const fetchNews = async () => {
		try {
			setIsLoading(true);

			const res = await newsGetAll();
			const _news = res.data;
			console.log("res: ", _news);

			setNews(_news);
		} catch (error) {
			handleError(error);
		} finally {
			setIsLoading(false);
		}
	};

	const createNews = async () => {
		try {
			setIsLoading(true);

			const res = await newsAdd(title, value);
			console.log("responseCreate : ", res.data);

			setTitle("");
			setValue("");
			fetchNews();
		} catch (error) {
			handleError(error);
		} finally {
			setIsLoading(false);
		}
	};

	const editNews = async () => {
		try {
			setIsLoading(true);

			const res = await newsEdit(selectedNews._id, title, value);
			console.log("responseEdit: ", res.data);

			setTitle("");
			setValue("");
			fetchNews();
			setSelectedNews({});
		} catch (error) {
			handleError(error);
		} finally {
			setIsLoading(false);
		}
	};

	const deleteNews = async () => {};

	const onSelectItem = (newsItem) => {
		console.log("selectedNews", newsItem);
		setSelectedNews(newsItem);
		setTitle(newsItem.title);
		setValue(newsItem.value);
		setImage(newsItem.image);
		navigation.navigate('DetailBarang', {
            itemId: newsItem.id,
            otherParam: newsItem.title,
			gambar : newsItem.image,
			harga : newsItem.price,
          });


	};

	useEffect(() => {
		fetchNews();
	}, []);

	const renderNews = ({ item }) => {
		const onPress = () => onSelectItem(item);
		const onDelete = () => {};

		return <NewsItem onDelete={onDelete} onPress={onPress} newsItem={item} />;
	};

	return (
		<View style={styles.container}>
			<View style={styles.header}>
				<Text style={styles.title}>Daftar Barang</Text>
			</View>
			{isLoading ? (
				<Text>Loading</Text>
			) : (
				<FlatList
					data={news}
					keyExtractor={(item) => item.id}
					renderItem={renderNews}
					onPress={onSelectItem}
				/>
			)}
			
			{/* <View style={styles.form}>
				<Text>Post Data</Text>
				<TextInput
					placeholder="Masukan Judul Berita"
					style={styles.input}
					value={title}
					onChangeText={setTitle}
				/>
				<TextInput
					placeholder="Masukan Isi Berita"
					style={styles.input}
					value={value}
					onChangeText={setValue}
				/>
				{selectedNews._id ? (
					<Button
						title="Update"
						onPress={() =>
							editNews({
								_id: selectedNews._id,
								title,
								value,
							})
						}
					/>
				) : (
					<Button title="Create" onPress={() => createNews(title, value)} />
				)}
				<Button title="SignOut" onPress={() => {}} />
			</View> */}
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "white",
		paddingHorizontal: 20,
	},
	header: {
		alignItems: "center",
	},
	title: {
		padding: 20,
		color: "black",
		fontSize: 20,
		fontWeight: "bold",
	},
	form: {
		paddingVertical: 20,
	},
	input: {
		borderWidth: 1,
		paddingVertical: 10,
		paddingHorizontal: 5,
		borderRadius: 6,
		marginBottom: 10,
	},
	contentNews: {
		backgroundColor: "grey",
		paddingVertical: 10,
	},
});
